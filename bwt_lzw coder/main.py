import json


def bwt_code(encode_str):
    str_to_encode = list(encode_str)
    move_list = []

    ''' Move list - list of all available permutations for string '''
    for i in range(len(str_to_encode)):
        loop_prefix = encode_str[-1] + encode_str[:-1]
        encode_el = ''.join(loop_prefix)
        encode_str = encode_el
        move_list.append(encode_el)
        i += 1

    sorted_ml = sorted(move_list)
    bwt_str = ''

    print(move_list)
    print(sorted_ml)

    original_str = move_list[-1]
    original_str_index = 0

    ''' BWT string - returns last characters of each string in Move list (alphabetical sorted) '''
    for i in range(len(str_to_encode)):
        if sorted_ml[i] == original_str:
            original_str_index = i

        element = sorted_ml[i]
        last = element[- 1]
        i += 1
        bwt_str += str(last)

    ''' Function returns 2 values - BWT encoded string [0] and original string position [1] '''
    return bwt_str, original_str_index


def lzw_compress(bwt_encoded):
    """Compress a string to a list of output symbols."""

    '''Create dict with alphabet'''
    dict_size = 0
    dictionary = {}

    ct = 0
    for i in range(len(bwt_encoded)):
        if bwt_encoded[i] not in dictionary:
            dictionary[bwt_encoded[i]] = ct
            ct += 1
            dict_size += 1

    '''write this dict in file to further decoding'''
    with open("alphabet.txt", "w") as file:
        file.write(json.dumps(dictionary))

    ''' Add to alphabet dict new values(character combinations) '''
    w = ""
    int_result = []
    for char in bwt_encoded:
        wc = w + char
        if wc in dictionary:
            w = wc
        else:
            int_result.append(dictionary[w])
            ''' Add combination to dict '''
            dictionary[wc] = dict_size
            dict_size += 1
            w = char

    if w:
        int_result.append(dictionary[w])
    ''' переводим алфавит в двоичный восьмибитный'''
    bin_result = []

    for elem in int_result:
        bin_elem = (bin(elem)[2::])
        while len(bin_elem) < 8:
            bin_elem = '0' + bin_elem
        bin_result.append(bin_elem)

    return bin_result


def main():
    text = input('Enter text to encode: ')
    encoded_text, original_pos = bwt_code(text)
    compressed_text = lzw_compress(encoded_text)
    with open("result.txt", "w") as file:
        file.write((json.dumps(compressed_text)) + '\n')
        file.write(json.dumps(original_pos))


if __name__ == '__main__':
    main()
