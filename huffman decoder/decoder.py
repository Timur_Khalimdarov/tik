class NodeTree:
    def __init__(self, left=None, right=None):
        self.left = left
        self.right = right

    def children(self):
        return self.left, self.right

    def __str__(self):
        return f'{self.left}, {self.right}'


def make_tree(nodes):
    while len(nodes) > 1:
        (key1, c1) = nodes[-1]
        (key2, c2) = nodes[-2]
        nodes = nodes[:-2]
        node = NodeTree(key1, key2)
        nodes.append((node, c1 + c2))
        nodes = sorted(nodes, key=lambda x: x[1], reverse=True)
    return nodes[0][0]


def huffman_code_tree(node, bin_string=''):
    if type(node) is str:
        return {bin_string: node}
    (l, r) = node.children()
    d = dict()
    d.update(huffman_code_tree(l, bin_string + '0'))
    d.update(huffman_code_tree(r, bin_string + '1'))
    return d


def huffman_decode(mapping, text):
    decoded_str = ""
    while text:
        for k in mapping:
            if text.startswith(k):
                decoded_str += mapping[k]
                text = text[len(k):]
    return decoded_str


def main():

    encoded = input('Введите строку для декодирования:\n')
    with open('test.txt', 'r') as f:
        alphabet = list(f.readline().strip('\n'))
        probabilities = [float(prob) for prob in f.readline().split()]

    mapping = sorted(zip(alphabet, probabilities), key=lambda x: x[1], reverse=True)
    node = make_tree(mapping)
    code_tree = huffman_code_tree(node)
    decoded = huffman_decode(code_tree, encoded)
    print(decoded)


if __name__ == '__main__':
    main()

